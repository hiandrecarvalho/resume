async function _loadData(language) {
	let response = await fetch("assets/data/resume-" + language + ".json");
	if (response.ok != true) return;

	try {
		return await response.json();
	} catch (error) {
		console.log(error);
	}
}

function _updateEducation(data) {
	let section = document.getElementById("education-section");
	section.children[0].textContent = data.header;

	let list = section.children[1];
	let listItems = list.getElementsByClassName("chronological-list__item")
	for (let index = 0; index < data.entries.length; index++) {
		const entry = data.entries[index];

		let entryHeader = listItems[index].children[0];
		entryHeader.children[0].textContent = entry.startDate;
		entryHeader.children[1].textContent = entry.endDate;

		let entryTitle = listItems[index].children[1];
		entryTitle.lastChild.textContent = " - " + entry.title.degree;

		let entryTags = listItems[index].children[2].children[0];
		for (let index = 0; index < entry.tags.length; index++) {
			entryTags.children[index].textContent = entry.tags[index];
		}
	}
}

function _updateFooter(data) {
	let resumeLocationMessage = document.getElementById("resume-location-message");
	resumeLocationMessage.textContent = data.resumeLocationMessage;

	let codeLocationMessage = document.getElementById("code-location-message");
	codeLocationMessage.textContent = data.codeLocationMessage;
}

function _updateLanguages(data) {
	let section = document.getElementById("language-section");
	section.children[0].textContent = data.header;

	let list = section.children[1];
	for (let index = 0; index < data.languages.length; index++) {
		list.children[index * 2].textContent = data.languages[index].term;
	}
}

function _updateIntroduction(data) {
	let section = document.getElementById("introduction-section");
	section.children[1].textContent = data.jobTitle;
}

function _updatePersonalInfo(data) {
	let section = document.getElementById("personal-info-section");
	section.children[0].textContent = data.header;

	let list = section.children[1];
	for (let index = 0; index < data.personalInfo.length; index++) {
		list.children[index * 2].textContent = data.personalInfo[index].term;
	}
}

function _updateLanguageSelect(language) {
	let select = document.getElementById("language-select");
	for (let index = 0; index < select.children.length; index++) {
		const option = select.children[index];
		if (option.value == language) {
			option.setAttribute("selected", true);
			return;
		}
	}
}

function _updateResume(data) {
	_updateIntroduction(data.introductionSection);

	_updatePersonalInfo(data.personalInfoSection);

	_updateTechnicalSkills(data.technicalSkillsSection);

	_updateLanguages(data.languageSection);

	_updateFooter(data.footer);

	_updateWorkExperience(data.workExperienceSection);

	_updateEducation(data.educationSection);
}

function _updateTechnicalSkills(data) {
	let section = document.getElementById("technical-skills-section");
	section.children[0].textContent = data.header;

	let tags = section.children[2];
	for (let index = 0; index < data.tags.length; index++) {
		tags.children[index].textContent = data.tags[index];
	}
}

function _updateWorkExperience(data) {
	let section = document.getElementById("work-experience-section");
	section.children[0].textContent = data.header;

	let list =  document.getElementsByClassName("job");
	for (let index = 0; index < data.entries.length; index++) {
		const entry = data.entries[index];

		let entryHeaderElement = list[index].children[0];
		entryHeaderElement.children[0].textContent = entry.startDate;
		if (entryHeaderElement.children.length == 2) {
			entryHeaderElement.children[1].textContent = entry.endDate;
		} else {
			entryHeaderElement.lastChild.textContent = " - " + entry.endDate;
		}

		let entryTitleElement = list[index].children[1];
		entryTitleElement.lastChild.textContent = entry.title;

		let entryProjectsElement = list[index].children[2];
		for (let index = 0; index < entry.projects.length; index++) {
			const project = entry.projects[index];

			let projectElement = entryProjectsElement.children[index];
			projectElement.children[0].textContent = project.title;

			for (let index = 0; index < project.details.length; index++) {
				projectElement.children[1].children[index].textContent =
					project.details[index];
			}

			for (let index = 0; index < project.tags.length; index++) {
				projectElement.children[2].children[index].textContent =
					project.tags[index];
			}
		}
	}
}

async function changeLanguage(language) {
	localStorage.setItem("language", language);

	let data = await _loadData(language);
	if (data == undefined) return alert("Something went wrong!");

	_updateResume(data);
}

function detectLanguageOfUser() {
	let availableLanguages = ["en", "pt"];
	let languageOfUser = navigator.language.substring(0, 2);
	if (!availableLanguages.includes(languageOfUser)) return "en";

	return languageOfUser;
}

async function init() {
	let language = localStorage.getItem("language") || detectLanguageOfUser();
	_updateLanguageSelect(language);
	changeLanguage(language);
}
